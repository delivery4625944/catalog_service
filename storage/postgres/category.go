package postgres

import (
	"context"
	"database/sql"
	"delivery/catalog_service/genproto/catalog_service"
	"delivery/catalog_service/pkg/helper"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *CategoryRepo {
	return &CategoryRepo{
		db: db,
	}
}
func (r *CategoryRepo) Create(ctx context.Context, req *catalog_service.CategoryCreateReq) (*catalog_service.CategoryCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO categories(
		id,
		title,
		images,
		parent_id
	) VALUES($1, $2, $3, $4);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Title,
		req.Images,
		helper.NewNullString(req.ParentId),
	)
	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &catalog_service.CategoryCreateResp{Msg: "category created with id: " + id}, nil
}

func (r *CategoryRepo) GetList(ctx context.Context, req *catalog_service.CategoryGetListReq) (*catalog_service.CategoryGetListResp, error) {
	var (
		filter   = " WHERE deleted_at IS NULL "
		offsetQ  = " OFFSET 0;"
		limit    = " LIMIT 10 "
		offset   = (req.Page - 1) * req.Limit
		count    int
		parentId sql.NullString
	)

	s := `
	SELECT 
		id,
		title,
		images,
		parent_id
	FROM 
		categories `

	if req.Search != "" {
		filter += ` AND title ILIKE ` + "'%" + req.Search + "%' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM categories` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &catalog_service.CategoryGetListResp{}
	for rows.Next() {
		var category = catalog_service.Category{}
		err := rows.Scan(
			&category.Id,
			&category.Title,
			&category.Images,
			&parentId,
		)

		if err != nil {
			return nil, err
		}
		resp.Categories = append(resp.Categories, &catalog_service.Category{
			Id:       category.Id,
			Title:    category.Title,
			Images:   category.Images,
			ParentId: parentId.String,
		})
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *CategoryRepo) GetById(ctx context.Context, req *catalog_service.CategoryIdReq) (*catalog_service.Category, error) {
	query := `
    SELECT 
        id,
        tite,
		images,
        parent_id  
    FROM 
		categories 
    `

	var (
		category = catalog_service.Category{}
		parentId sql.NullString
	)
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&category.Id,
		&category.Title,
		&category.Images,
		&parentId,
	); err != nil {
		return nil, err
	}

	return &catalog_service.Category{
		Id:       category.Id,
		Title:    category.Title,
		Images:   category.Images,
		ParentId: parentId.String,
	}, nil
}

func (r *CategoryRepo) Update(ctx context.Context, req *catalog_service.CategoryUpdateReq) (*catalog_service.CategoryUpdateResp, error) {
	query := `
    UPDATE categories 
    SET 
        title=$2,
		images=$3,
        parent_id=$4
    WHERE 
		id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.Title,
		req.Images,
		helper.NewNullString(req.ParentId),
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}
	return &catalog_service.CategoryUpdateResp{Msg: "OK"}, nil
}

func (r *CategoryRepo) Delete(ctx context.Context, req *catalog_service.CategoryIdReq) (*catalog_service.CategoryDeleteResp, error) {
	query := `
    DELETE FROM 
		categories 
	WHERE
		id = $1
	`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &catalog_service.CategoryDeleteResp{Msg: "OK"}, nil
}
