package postgres

import (
	"context"
	"delivery/catalog_service/genproto/catalog_service"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *ProductRepo {
	return &ProductRepo{
		db: db,
	}
}

func (r *ProductRepo) Create(ctx context.Context, req *catalog_service.ProductCreateReq) (*catalog_service.ProductCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO products (
		id,
		title,
		description,
		photos,
		order_number,
		price,	
		category_id
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Title,
		req.Description,
		req.Photos,
		req.OrderNumber,
		req.Price,
		req.CategoryId,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &catalog_service.ProductCreateResp{Msg: "product created with id: " + id}, nil
}

func (r *ProductRepo) GetList(ctx context.Context, req *catalog_service.ProductGetListReq) (resp *catalog_service.ProductGetListResp, err error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		title,
		descriptoin,
		photos,
		order_number,
		price,
		category_id 
	FROM 
		products `

	if req.Title != "" {
		filter += ` AND title ILIKE ` + "'%" + req.Title + "%' "
	}
	if req.Category != "" {
		filter += ` AND category_id = ` + "'" + req.Category + "' "  
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM products` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp = &catalog_service.ProductGetListResp{}
	for rows.Next() {
		var product = catalog_service.Product{}
		err := rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photos,
			&product.OrderNumber,
			&product.Price,
			&product.CategoryId,
		)

		if err != nil {
			return nil, err
		}
		resp.Products = append(resp.Products, &product)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *ProductRepo) GetById(ctx context.Context, req *catalog_service.ProductIdReq) (res *catalog_service.Product, err error) {
	query := `
	SELECT 
		id,
		title,
		description,
		photos,
		order_number,
		price,
		category_id
	FROM products 
	WHERE id=$1 `

	var product = catalog_service.Product{}
	if err = r.db.QueryRow(ctx, query, req.Id).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photos,
		&product.OrderNumber,
		&product.Price,
		&product.CategoryId,
	); err != nil {
		return nil, err
	}

	return &product, nil
}

func (r *ProductRepo) Update(ctx context.Context, req *catalog_service.ProductUpdateReq) (resp *catalog_service.ProductUpdateResp, err error) {
	query := `
	UPDATE products 
	SET 
		title=$2,
		description=$3,
		photos=$4,
		order_number=$5,
		price=$6,
		category_id=$7 
	WHERE 
		id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.Title,
		req.Description,
		req.Photos,
		req.OrderNumber,
		req.Price,
		req.CategoryId,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}
	return &catalog_service.ProductUpdateResp{Msg: "OK"}, nil
}

func (r *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.ProductDeleteResp, error) {
	query := `
    DELETE FROM 
		products
	WHERE
		id = $1
	`
	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &catalog_service.ProductDeleteResp{Msg: "OK"}, nil
}
