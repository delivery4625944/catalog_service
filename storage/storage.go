package storage

import (
	"context"
	"delivery/catalog_service/genproto/catalog_service"
)

type StorageI interface {
	CloseDB()
	Product() ProductRepoI
	Category() CategoryRepoI
}

type ProductRepoI interface {
	Create(ctx context.Context, req *catalog_service.ProductCreateReq) (*catalog_service.ProductCreateResp, error)
	GetList(ctx context.Context, req *catalog_service.ProductGetListReq) (resp *catalog_service.ProductGetListResp, err error)
	GetById(ctx context.Context, req *catalog_service.ProductIdReq) (res *catalog_service.Product, err error)
	Update(ctx context.Context, req *catalog_service.ProductUpdateReq) (resp *catalog_service.ProductUpdateResp, err error)
	Delete(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.ProductDeleteResp, error)
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *catalog_service.CategoryCreateReq) (*catalog_service.CategoryCreateResp, error)
	GetList(ctx context.Context, req *catalog_service.CategoryGetListReq) (*catalog_service.CategoryGetListResp, error)
	GetById(ctx context.Context, req *catalog_service.CategoryIdReq) (*catalog_service.Category, error)
	Update(ctx context.Context, req *catalog_service.CategoryUpdateReq) (*catalog_service.CategoryUpdateResp, error)
	Delete(ctx context.Context, req *catalog_service.CategoryIdReq) (*catalog_service.CategoryDeleteResp, error)
}
