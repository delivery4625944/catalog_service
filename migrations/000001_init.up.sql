CREATE TYPE "product_type" AS ENUM (
  'modifier',
  'product'
);

CREATE TABLE "categories" (
  "id" uuid PRIMARY KEY,
  "title" varchar,
  "images" varchar,
  "active" bool DEFAULT true,
  "parent_id" uuid,
  "order_number" serial
);

CREATE TABLE "products" (
  "id" uuid PRIMARY KEY,
  "title" varchar,
  "description" varchar,
  "photos" varchar,
  "order_number" integer,
  "active" bool DEFAULT true,
  "type" product_type DEFAULT null,
  "price" float,
  "category_id" uuid
);

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "categories" ADD FOREIGN KEY ("parent_id") REFERENCES "categories" ("id");