package grpc

import (
	"delivery/catalog_service/config"
	"delivery/catalog_service/genproto/catalog_service"
	"delivery/catalog_service/grpc/client"
	"delivery/catalog_service/grpc/service"
	"delivery/catalog_service/pkg/logger"
	"delivery/catalog_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
